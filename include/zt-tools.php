<?php

/**
 * Tools class
 *
 * @author dimabresky
 */
class zt_tools {

    public static function get_zt_post_data($code) {

        $post_data = [];
        parse_str($_POST["post_data"], $post_data);
        if (!isset($post_data[$code]) || !is_array($post_data[$code]) || empty($post_data[$code])) {
            $post_data[$code] = WC()->session->get($code);
        } else {
            $arDesc = [];
            if (strlen($post_data[$code]["cityname"])) {
                $arDesc[] = \htmlspecialchars($post_data[$code]["cityname"]);
            }
            if (strlen($post_data[$code]["partner"])) {
                $arDesc[] = \htmlspecialchars($post_data[$code]["partner"]);
            }
            if (strlen($post_data[$code]["address"])) {
                $arDesc[] = \htmlspecialchars($post_data[$code]["address"]);
            }
            if (strlen($post_data[$code]["phone"])) {
                $arDesc[] = \htmlspecialchars($post_data[$code]["phone"]);
            }
            $post_data[$code]["desc"] = implode(", ", $arDesc);

            WC()->session->set($code, $post_data[$code]);
        }

        return $post_data[$code];
    }

    /**
     * @return boolean
     */
    public static function is_cart() {
        $page = trim($_SERVER["REQUEST_URI"], "/");

        return $page === "cart" || strpos($page, "?wc-ajax=update_shipping_method") !== false;
    }

    /**
     * @return array
     */
    public static function get_order_statuses() {

        return [
            "wc-zt-za" => [
                'without_prefix_status_code' => 'zt-za',
                'xml_id' => 1,
                'label' => __('Забери товар: Ожидается привоз заказа', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zb" => [
                'without_prefix_status_code' => 'zt-zb',
                'xml_id' => 2,
                'label' => __('Забери товар: На сборке', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zс" => [
                'without_prefix_status_code' => 'zt-zc',
                'xml_id' => 3,
                'label' => __('Забери товар: Заказ готов к выдаче', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zd" => [
                'without_prefix_status_code' => 'zt-zd',
                'xml_id' => 5,
                'label' => __('Забери товар: Заказ отменен покупателем', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-ze" => [
                'without_prefix_status_code' => 'zt-ze',
                'xml_id' => 7,
                'label' => __('Забери товар: Отменен по сроку хранения', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zf" => [
                'without_prefix_status_code' => 'zt-zf',
                'xml_id' => 10,
                'label' => __('Забери товар: Заказ выдан', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zj" => [
                'without_prefix_status_code' => 'zt-zj',
                'xml_id' => 11,
                'label' => __('Забери товар: Отменен, ожидает возврата', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zh" => [
                'without_prefix_status_code' => 'zt-zh',
                'xml_id' => 12,
                'label' => __('Забери товар: Заказ подготовлен на возврат', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zi" => [
                'without_prefix_status_code' => 'zt-zi',
                'xml_id' => 30,
                'label' => __('Забери товар: Ожидает транспортировки', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zg" => [
                'without_prefix_status_code' => 'zt-zg',
                'xml_id' => 31,
                'label' => __('Забери товар: Заказ передан курьеру', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zk" => [
                'without_prefix_status_code' => 'zt-zk',
                'xml_id' => 32,
                'label' => __('Забери товар: Заказ передан партнеру', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zl" => [
                'without_prefix_status_code' => 'zt-zl',
                'xml_id' => 33,
                'label' => __('Забери товар: Транспортировка', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zm" => [
                'without_prefix_status_code' => 'zt-zm',
                'xml_id' => 34,
                'label' => __('Забери товар: Отменен. Ожидает транспортировки', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zn" => [
                'without_prefix_status_code' => 'zt-zn',
                'xml_id' => 34,
                'label' => __('Забери товар: Отменен, передан курьеру', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zo" => [
                'without_prefix_status_code' => 'zt-zo',
                'xml_id' => 39,
                'label' => __('Забери товар: Выполнен', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zp" => [
                'without_prefix_status_code' => 'zt-zp',
                'xml_id' => 40,
                'label' => __('Забери товар: Ожидает забора', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zq" => [
                'without_prefix_status_code' => 'zt-zq',
                'xml_id' => 48,
                'label' => __('Забери товар: Ожидает расчета', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zr" => [
                'without_prefix_status_code' => 'zt-zr',
                'xml_id' => 49,
                'label' => __('Забери товар: Подготовлен на возврат', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zs" => [
                'without_prefix_status_code' => 'zt-zs',
                'xml_id' => 50,
                'label' => __('Забери товар: Возвращен', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zt" => [
                'without_prefix_status_code' => 'zt-zt',
                'xml_id' => 51,
                'label' => __('Забери товар: Возвращен на склад', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zu" => [
                'without_prefix_status_code' => 'zt-zu',
                'xml_id' => 99,
                'label' => __('Забери товар: Утерян при транспортировке', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ],
            "wc-zt-zy" => [
                'without_prefix_status_code' => 'zt-zy',
                'xml_id' => 99,
                'label' => __('Забери товар: Отгружен', 'zt'),
                'public' => true,
                'show_in_admin_status_list' => true,
                'show_in_admin_all_list' => true,
                'exclude_from_search' => false,
                'label_count' => ''
            ]
        ];
    }

    /**
     * @param WC_Order $order
     * @return int
     */
    public static function get_service_id_by_order($order) {

        $method_id = current($order->get_items('shipping'))->get_method_id();
        return $method_id === "zt_pvz" ? 1 : 2;
    }

    public static function get_common_xml_request_string() {

        $login = get_option("zt_login");
        $client_api_id = get_option("zt_client_api_key");
        return "<client_name>$login</client_name><client_api_id>$client_api_id</client_api_id>";
    }

    /**
     * @param WC_Order $order
     * @return string
     */
    public static function create_request_body($order) {

        $data = $order->get_data();

        $zt_code = $zt_desc = "";
        foreach ($data["meta_data"] as $meta_data) {
            $meta_data_value = $meta_data->get_data();
            if ($meta_data_value["key"] === "zt_code") {
                $zt_code = $meta_data_value["value"];
            } elseif ($meta_data_value["key"] === "zt_desc") {
                $zt_desc = $meta_data_value["value"];
            }
        }

        $costumer_name = $data["billing"]["first_name"] . " " . $data["billing"]["last_name"];

        $service_id = self::get_service_id_by_order($order);

        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                <methodCall><methodName>add_new_order</methodName>" . self::get_common_xml_request_string() . "
                    <params>
                        <orders>
                            <item>
                                <order_id>{$order->get_id()}</order_id>
                                <int_number>{$order->get_id()}</int_number>
                                <service>$service_id</service>
                                <order_amount>{$data["total"]}</order_amount>
                                <d_price>{$data["total"]}</d_price>
                                <fio>{$costumer_name}</fio>
                                <phone>{$data["billing"]["phone"]}</phone>" . ($service_id !== 1 ? "<address>{$zt_desc}</address>" : "") . "
                                <comment></comment>
                                <final_pv>{$zt_code}</final_pv>
                                <weight>1</weight>
                                <goods></goods>
                            </item>
                        </orders>
                    </params>
                </methodCall>";
    }

    /**
     * @param WC_Order $order
     * @return string
     */
    public static function create_request_update_order_status_body($order) {
        $uid = $order->get_meta("zt_uid");

        if (!empty($uid)) {



            return "<?xml version=\"1.0\" encoding=\"utf-8\"?>
                    <methodCall><methodName>get_orders_by_id</methodName>" . self::get_common_xml_request_string() . "
                    <params>
                          <orders>
                                    <id>$uid</id>
                              </orders>
                    </params>
                    </methodCall>
                ";
        }

        return null;
    }

    /**
     * @param WC_Order $order
     * @return boolean
     */
    public static function need_call_request($order) {

        $status = $order->get_status();

        $uid = $order->get_meta("uid");

        $ext_zt_code = $order->get_meta("zt_code");

        if (!$uid && $status === "zt-zy" && strlen($ext_zt_code)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $body
     * @return mixed
     */
    public static function send_request($body) {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://lc.zaberi-tovar.ru/api/');
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'xml=' . $body);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Opera 10.00');
        $response = curl_exec($curl);
        curl_close($curl);

        return $response ? \simplexml_load_string($response) : null;
    }

    public static function __dump($var) {
        ob_start();
        echo "<pre>";
        print_R($var);
        echo "</pre>";
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/_log.txt", ob_get_clean());
    }

    /**
     * @param WC_Order $order
     */
    public static function create_ext_order($order) {

        $response = self::send_request(self::create_request_body($order));

        if ($response) {
            $params = $response->params;
            if ($params && strtolower($params->status) === "ok") {
                $orders = $params->orders;
                if ($orders) {
                    $item = $orders->item;
                    if ($item && $item->result) {
                        $order->update_meta_data('zt_uid', $item->result->__toString());
                        $order->save_meta_data();
                    }
                }
            }
        }
    }

    /**
     * @param WC_Order $order
     */
    public static function update_order_status($order) {
        $request_body = self::create_request_update_order_status_body($order);
        if ($request_body) {
            $response = self::send_request($request_body);

            if ($response) {
                $params = $response->params;
                if ($params && strtolower($params->status) === "ok") {
                    $orders = $params->orders;
                    if ($orders) {
                        $item = $orders->item;
                        if ($item && $item->status) {
                            $status_xml_id = intval($item->status->__toString());
                            if ($status_xml_id > 0) {
                                $order->update_status(self::get_status_code_by_xml_id($status_xml_id));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param int $xml_id
     * @return string
     */
    public static function get_status_code_by_xml_id($xml_id) {
        foreach (self::get_order_statuses() as $status_data) {
            if ($status_data["xml_id"] === $xml_id) {
                return $status_data["without_prefix_status_code"];
            }
        }
    }

    public static function change_shipping_zt_pvz_name($shipping_name) {
        $packages = WC()->shipping->get_packages();
        foreach ($packages as $package) {
            if (isset($package['rates'])) {

                if (isset($package['rates']['zt_pvz']) &&
                        strpos($shipping_name, $package['rates']['zt_pvz']->get_label()) !== false) {
                    if (strpos($shipping_name, "id=\"zt_pvz\"") === false) {

                        $shipping_name .= include "views/pvz.php";
                        break;
                    }
                }
            }
        }
        return $shipping_name;
    }

    public static function change_shipping_zt_courier_name($shipping_name) {

        $packages = WC()->shipping->get_packages();
        foreach ($packages as $package) {
            if (isset($package['rates'])) {

                if (isset($package['rates']['zt_courier']) &&
                        strpos($shipping_name, $package['rates']['zt_courier']->get_label()) !== false) {
                    if (strpos($shipping_name, "id=\"zt_courier\"") === false) {
                        $shipping_name .= include "views/courier.php";
                        break;
                    }
                }
            }
        }
        return $shipping_name;
    }

}
