<?php

/*
 * zt order custom button on admin page
 */


add_action('woocommerce_order_item_add_action_buttons', 'zt_update_status_order_custom_button', 10, 1);

/**
 * @param WC_Order $order
 */
function zt_update_status_order_custom_button($order) {

    $method_id = current($order->get_items('shipping'))->get_method_id();
    $meta_data = $order->get_data()["meta_data"];
    $uid = null;
    foreach ($meta_data as $meta) {
        $meta_data_value = $meta->get_data();
        if ($meta_data_value["key"] === "zt_uid") {
            $uid = $meta_data_value["value"];
            break;
        }
    }
    if (in_array($method_id, ["zt_pvz", "zt_courier"]) && $uid) {
        echo '<button type="button" id="zt-update-status-btn" onclick="function __zt_update_status () {var input=document.createElement(\'input\');input.type=\'hidden\';input.name=\'zt_update_order_status\';input.value=\'Y\';document.querySelector(\'#zt-update-status-btn\').after(input);document.post.submit();} __zt_update_status();" class="button generate-items">' . __('Забери товар: Запрос обновления статуса', 'zt') . '</button>';
    }
}

function zt_update_order_status($order_id, $post) {
    $slug = 'shop_order';
    if (is_admin()) {

        if ($slug != $post->post_type) {
            return;
        }
        if (isset($_POST['zt_update_order_status']) && $_POST['zt_update_order_status'] === "Y") {
            zt_tools::update_order_status(wc_get_order($order_id));
        }
    }
}

add_action('save_post', 'zt_update_order_status', 10, 3);
