<?php

/** init zt order statuses */

function zt_register_order_statuses ($order_statuses) {
    $order_statuses = array_merge($order_statuses, zt_tools::get_order_statuses());
    
    return $order_statuses;
}

add_filter('woocommerce_register_shop_order_post_statuses', 'zt_register_order_statuses');

/**
 * @param array $order_statuses
 * @return array
 */
function zt_add_order_statuses ($order_statuses) {
    $statuses = zt_tools::get_order_statuses();
    foreach ($statuses as $code => $data) {
        $order_statuses[$code] = $data["label"];
    }
    
    return $order_statuses;
}

add_filter( 'wc_order_statuses', 'zt_add_order_statuses' );