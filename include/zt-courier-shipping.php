<?php

/** init zt shipping methods */
function zt_courier_shipping_method() {
    if (!class_exists('ZTCourier_Shipping_Method')) {

        class ZTCourier_Shipping_Method extends WC_Shipping_Method {

            /**
             * Constructor for your shipping class
             *
             * @access public
             * @return void
             */
            public function __construct() {
                $this->id = 'zt_courier';
                $this->method_title = __('Забери товар: курьерская доставка', 'zt');
                $this->method_description = '';

                $this->availability = 'including';
                $this->countries = [
                    "RU" // only for Russian
                ];

                $this->init();

                $this->enabled = isset($this->settings['enabled']) ? $this->settings['enabled'] : 'yes';
                $this->title = isset($this->settings['title']) ? $this->settings['title'] : __('Забери товар: курьерская доставка', 'zt');
            }

            /**
             * Init your settings
             *
             * @access public
             * @return void
             */
            public function init() {
                // Load the settings API
                $this->init_form_fields();
                $this->init_settings();

                // Save settings in admin if you have any defined
                add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
            }

            /**
             * Define settings field for this shipping
             * @return void 
             */
            public function init_form_fields() {

                $this->form_fields = [
                    'enabled' => array(
                        'title' => __('Включена', 'zt'),
                        'type' => 'checkbox',
                        'description' => __('Включить данную доставку', 'zt'),
                        'default' => 'yes'
                    ),
                    'title' => array(
                        'title' => __('Заголовок', 'zt'),
                        'type' => 'text',
                        'description' => __('Название доставки для отображения на сайте', 'zt'),
                        'default' => __('Забери товар: курьерская доставка', 'zt')
                    ),
                ];
            }

            /**
             * This function is used to calculate the shipping cost. 
             * Within this function we can check for weights, dimensions and other parameters.
             *
             * @access public
             * @param array $package
             * @return void
             */
            public function calculate_shipping($package = []) {

                include_once "zt-tools.php";

                if (!zt_tools::is_cart()) {

                    $data = zt_tools::get_zt_post_data("zt_courier");

                    $rate = [
                        "id" => $this->id,
                        "label" => $this->title,
                        "cost" => $data["price"] ? $data["price"] : 0.00
                    ];

                    
                } else {
                    $rate = [
                        "id" => $this->id,
                        "label" => $this->title,
                        "cost" => ""
                    ];
                }
                
                $this->add_rate($rate);
            }

        }

    }
}

add_action('woocommerce_shipping_init', 'zt_courier_shipping_method');

function add_zt_courier_shipping_method($methods) {
    $methods[] = 'ZTCourier_Shipping_Method';
    return $methods;
}

add_filter('woocommerce_shipping_methods', 'add_zt_courier_shipping_method');
