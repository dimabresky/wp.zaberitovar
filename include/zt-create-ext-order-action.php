<?php

/*
 * zt create external order
 */

function zt_create_external_order_action($order_id) {

    $order = wc_get_order($order_id);

    if (zt_tools::need_call_request($order)) {
        zt_tools::create_ext_order($order);
    }
}

add_action('woocommerce_update_order', 'zt_create_external_order_action', 10, 1);

