<?php

/** init zt settings */

/**
 * @author dimabresky
 */
class zt_settings_page {

    public static function init() {
        add_filter('woocommerce_settings_tabs_array', __CLASS__ . '::add_settings', 50);
        add_action('woocommerce_settings_zt', __CLASS__ . '::settings_tab');
        add_action('woocommerce_update_options_zt', __CLASS__ . '::update_settings');
    }

    /**
     * @param array $settings_tabs
     * @return array
     */
    public static function add_settings($settings_tabs) {
        $settings_tabs['zt'] = __('Общие настройки сервиса доставки Забери товар', 'zt');
        return $settings_tabs;
    }

    public static function settings_tab() {
        woocommerce_admin_fields(self::get_settings());
    }

    public static function update_settings() {
        woocommerce_update_options(self::get_settings());
    }

    public static function get_settings() {
        $settings = array(
            'zt_login' => array(
                'name' => __('Логин', 'zt'),
                'type' => 'text',
                'desc' => '',
                'css' => 'display: block',
                'id' => 'zt_login'
            ),
            'zt_password' => array(
                'name' => __('Пароль', 'zt'),
                'type' => 'password',
                'desc' => '',
                'css' => 'display: block',
                'id' => 'zt_password'
            ),
            'zt_client_api_key' => array(
                'name' => __('Client api key', 'zt'),
                'type' => 'text',
                'desc' => '',
                'css' => 'display: block',
                'id' => 'zt_client_api_key'
            ),
            'zt_widget_api_key' => array(
                'name' => __('Widget api key', 'zt'),
                'type' => 'text',
                'desc' => '',
                'css' => 'display: block',
                'id' => 'zt_widget_api_key'
            )
        );
        return apply_filters('wc_settings_zt', $settings);
    }

}

zt_settings_page::init();
