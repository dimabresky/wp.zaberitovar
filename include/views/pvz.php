<?php

if (zt_tools::is_cart()) {
    return "";
}


ob_start();
$arData = zt_tools::get_zt_post_data("zt_pvz");
?>
<div class="zt-pvz-delivery-description-block">
    <input name="zt_pvz[cod]" value="<?= \htmlspecialchars($arData["cod"]) ?>" type="hidden">
    <input name="zt_pvz[price]" value="<?= \htmlspecialchars($arData["price"]) ?>" type="hidden">
    <input name="zt_pvz[srok]" value="<?= \htmlspecialchars($arData["srok"]) ?>" type="hidden">
    <input name="zt_pvz[cityname]" value="<?= \htmlspecialchars($arData["cityname"]) ?>" type="hidden">
    <input name="zt_pvz[phone]" value="<?= \htmlspecialchars($arData["phone"]) ?>" type="hidden">
    <input name="zt_pvz[work_time]" value="<?= \htmlspecialchars($arData["work_time"]) ?>" type="hidden">
    <input name="zt_pvz[pvz_name]" value="<?= \htmlspecialchars($arData["pvz_name"]) ?>" type="hidden">
    <input name="zt_pvz[address]" value="<?= \htmlspecialchars($arData["address"]) ?>" type="hidden">

    <?php
    $title = __('Выбрать пункт самовывоза', 'zt');
    if (!empty($arData["cod"])):
        $title = __('Изменить пункт самовывоза', 'zt');
        echo sprintf(__('<small>Пункт самовывоза: %s</small>', 'zt'), $arData["desc"]);
        ?>
        <br>
        <br>
    <?php endif ?>
    <small><a id="zt_pvz" href="javascript:Zaberitovar.pvzOpenWidget('<?= get_option("zt_widget_api_key") ?>');"><?= $title ?></a></small>
</div>

<?php

include_once "script_js.php";

return ob_get_clean()?>