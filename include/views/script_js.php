<script>
    /* 
     * @package wp.zaberitovar
     */
    if (!window.Zaberitovar) {
        window.Zaberitovar = {

            __widgetPvz: null,
            __widgetCourier: null,
            __createHiddenInput: function (name, value) {
                var input = document.createElement("input");
                input.name = name;
                input.value = value;
                input.type = "hidden";
                return input;
            },
            __selectDeliveryPoint: function (result) {

                var data = result.data;

                var cart_calculator_form = document.querySelector(".woocommerce-shipping-calculator");

                var input = null;

                // самовывоз
                if (data.service === "1") {

                    document.querySelector("input[name=\"zt_pvz[cod]\"").value = data.cod;
                    document.querySelector("input[name=\"zt_pvz[price]\"").value = data.price;
                    document.querySelector("input[name=\"zt_pvz[srok]\"").value = data.srok;
                    document.querySelector("input[name=\"zt_pvz[cityname]\"").value = data.cityname;
                    document.querySelector("input[name=\"zt_pvz[phone]\"").value = data.phone;
                    document.querySelector("input[name=\"zt_pvz[work_time]\"").value = data.work_time;
                    document.querySelector("input[name=\"zt_pvz[pvz_name]\"").value = data.pvz_name;
                    document.querySelector("input[name=\"zt_pvz[address]\"").value = data.address;


                    window.Zaberitovar.__widgetPvz.hideOverlay();
                    window.Zaberitovar.__widgetPvz.hideContainer();
                }

                // курьер
                if (data.service === "2") {


                    document.querySelector("input[name=\"zt_courier[cod]\"").value = data.cod;
                    document.querySelector("input[name=\"zt_courier[price]\"").value = data.price;
                    document.querySelector("input[name=\"zt_courier[srok]\"").value = data.srok;
                    document.querySelector("input[name=\"zt_courier[cityname]\"").value = data.cityname;
                    document.querySelector("input[name=\"zt_courier[partner]\"").value = data.partner;

                    window.Zaberitovar.__widgetCourier.hideOverlay();
                    window.Zaberitovar.__widgetCourier.hideContainer();


                }

                document.body.dispatchEvent(new Event('update_checkout'));
            },
            pvzOpenWidget: function (widget_api_key) {
                window.Zaberitovar.__widgetPvz.open(window.Zaberitovar.__selectDeliveryPoint, widget_api_key, 0);
            },
            courierOpenWidget: function (widget_api_key) {
                window.Zaberitovar.__widgetCourier.open(window.Zaberitovar.__selectDeliveryPoint, widget_api_key, 0);
            },

            loadJs: function () {

                var script_widjet = document.createElement("script");
                var script_widjet_cour = document.createElement("script");

                script_widjet.src = "//api.zaberi-tovar.ru/widget/pvz.js";
                script_widjet.onload = function () {
                    window.Zaberitovar.__widgetPvz = zt;
                };

                script_widjet_cour.src = "//api.zaberi-tovar.ru/widget-cour/pvz.js";
                script_widjet_cour.onload = function () {
                    window.Zaberitovar.__widgetCourier = zt_cour;
                };

                document.body.appendChild(script_widjet);
                document.body.appendChild(script_widjet_cour);
            }


        };

        window.Zaberitovar.loadJs();

        onmessage = function (e) {
            window.Zaberitovar.__selectDeliveryPoint(e);
        };
    }
</script>

