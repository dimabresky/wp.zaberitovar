<?php

if (zt_tools::is_cart()) {
    return "";
}

ob_start();
$arData = zt_tools::get_zt_post_data("zt_courier");

?>
<div class="zt-courier-delivery-description-block">
    <input name="zt_courier[cod]" value="<?= \htmlspecialchars($arData["cod"]) ?>" type="hidden">
    <input name="zt_courier[cityname]" value="<?= \htmlspecialchars($arData["cityname"]) ?>" type="hidden">
    <input name="zt_courier[srok]" value="<?= \htmlspecialchars($arData["srok"]) ?>" type="hidden">
    <input name="zt_courier[partner]" value="<?= \htmlspecialchars($arData["partner"]) ?>" type="hidden">
    <input name="zt_courier[price]" value="<?= \htmlspecialchars($arData["price"]) ?>" type="hidden">
    <?php
    $title = __('Выбрать курьерскую службу', 'zt');
    if (!empty($arData["cod"])):
        $title = __('Выбрать курьерскую службу', 'zt');
        echo sprintf(__('<small>Курьерская служба: %s</small>', 'zt'), $arData["desc"]);
        ?>
        <br>
        <br>
    <?php endif ?>
        
    <small><a id="zt_courier" href="javascript:Zaberitovar.courierOpenWidget('<?= get_option("zt_widget_api_key") ?>');"><?= $title ?></a></small>

</div>

<?php
include_once "script_js.php";
return ob_get_clean()?>
