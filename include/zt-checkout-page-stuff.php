<?php

/** init zt checkout page stuff */
function zt_add_checkout_page_stuff($shipping_name) {
    $packages = WC()->shipping->get_packages();

    $chosen_methods = WC()->session->get('chosen_shipping_methods');

    if (is_array($chosen_methods)) {
        
        if (in_array('zt_pvz', $chosen_methods)) {
            return zt_tools::change_shipping_zt_pvz_name($shipping_name);
        } elseif (in_array('zt_courier', $chosen_methods)) {
            return zt_tools::change_shipping_zt_courier_name($shipping_name);
        }
        
        return $shipping_name;
        
    }

}

add_filter('woocommerce_cart_shipping_method_full_label', 'zt_add_checkout_page_stuff');

/**
 * @param int $order_id
 */
function zt_add_meta_data($order_id) {

    $chosen_methods = WC()->session->get('chosen_shipping_methods');
    if (is_array($chosen_methods)) {

        $data = null;
        if (in_array('zt_pvz', $chosen_methods)) {

            $data = zt_tools::get_zt_post_data("zt_pvz");
        } elseif (in_array('zt_courier', $chosen_methods)) {

            $data = zt_tools::get_zt_post_data("zt_courier");
        }

        if (!empty($data)) {
            $order = wc_get_order($order_id);
            $order->update_meta_data('zt_code', $data["cod"]);
            $order->update_meta_data('zt_desc', $data["desc"]);
            $order->save_meta_data();
        }
    }
}

add_action('woocommerce_checkout_update_order_meta', 'zt_add_meta_data');

add_action('woocommerce_checkout_update_order_review', 'checkout_update_refresh_shipping_methods', 10, 1);
function checkout_update_refresh_shipping_methods() {
    // reset cache for recalculation
    WC()->session->set( 'shipping_for_package_0', false );
    WC()->session->set( 'shipping_for_package_zt_pvz', false );
    WC()->session->set( 'shipping_for_package_zt_courier', false );
}
