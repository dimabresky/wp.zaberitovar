<?php

/**
 * Plugin Name: Zaberitovar Shipping
 * Plugin URI: https://gitlab.com/dimabresky/wp.zaberitovar
 * Description: Сервис доставки Забери товар
 * Version: 1.0.0
 * Author: ИП Бреский Дмитрий Игоревич
 * Author URI: https://gitlab.com/dimabresky
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Network: true
 */
if (!defined('WPINC') || !function_exists("curl_init") || !function_exists("simplexml_load_string")) {

    die;
}

/*
 * Check if WooCommerce is active
 */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    
    include_once "include/zt-tools.php";
    include_once "include/zt-pvz-shipping.php";
    include_once "include/zt-courier-shipping.php";
    include_once "include/zt-checkout-page-stuff.php";
    include_once "include/zt-settings.php";
    include_once "include/zt-order-statuses.php";
    include_once "include/zt-create-ext-order-action.php";
    include_once "include/zt-order-custom-button.php";

}